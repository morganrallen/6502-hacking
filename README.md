# 6502-hacking

A project to work with [Ben Eater's 6502] computer kit, replacing the clock module
and EEPROM, amongst other things.

\> HACKING <

This is very much work in progress but does manage to demonstrate several things.
It's made up of two parts. `6502-contoller` is an Arduino firmware for interacting
with the 6502 and `6502-loader` is a python script to interact with the Arduino.

![test setup with Nucleo F401RE](test-setup.jpg "Test Setup")
![6502-loader in action!](in-action.png "6502-loader in action!")

## 6502-contoller
This Arduino firmware started off fairly similarly to Ben's monitor firmware but I
needed a couple additional things. As I didn't have the clock module it also provided
a clock signal from the start. It can also be tied to the 6502 `RSTb` line and control
the reset signal. I also opted to not buy the EEPROM reader for several reasons.
One, I don't have the money and additionally I just hate the idea of having
to pull a chip to program it, place it back in, blah, blah. I spend 20 years in web
development damn it! CTRL-R is my compiler! So the firmware can load the entire ROM
into memory, read the `ADDRESS` lines coming off the 6502 and provided the addressed byte
on the `DATA` lines.

This second feature, while being generally great and speeding up development does have
one major hiccup. It's requires not only an Arduino with many IO, like a Mega, it
also requires a device with over about 60kb of memory... unlike a Mega. For this project
I'm using an inexpressive STMicro Nucleo F401RE devboard. While inexpressive these
are still not as accessible as a Mega for some people. The originally I had wanted to
request each byte over serial as the address lines change but could not manage to make
this work. Now that the entire project is working better and my understanding has
improved I *do* think it's possible and might be worth revisiting.

## 6502-loader.py
This part interacts with the Arduino firmware. In it's basic flow it handles several things.

* Stopping the clock
* Putting the 6502 into reset
* Loading the ROM into the Arduinos memory
* Restarting the clock
* Bringing the 6502 out of reset
* Runs monitor mode

At this point the script can be exited and the 6502 will continue to run. A normal terminal
can be opened to stop the clock and use single stepping from the devboard.

## Usage
The Arduino firmware should be fairly straight forward. If you're using a similar Nucleo
devboard you should be able to install the STM32 boards from the Board Manager. The `6502-loader`
requires `python3`, `virtualenv` and `pip`. It can be setup and run as follows.

```
git clone https://gitlab.com/morganrallen/6502-hacking.git
cd 6502-hacking
virtualenv .
. bin/activate
./6502-loader --port /dev/ttyACM0 a.out --start
```

[Ben Eater's 6502]: https://eater.net/6502
